const controllers = require('../controllers');

module.exports = (server) => {
  server.get('/users', controllers.getUsers);
  server.get('/users/:username', controllers.getUser);
  server.post('/users', controllers.createUser);

  server.get('/projects', controllers.getProjects);
  server.get('/projects/:project', controllers.getProject);
  server.post('/projects', controllers.createProject);

  server.get('/log/:project', controllers.getLogs);
  server.post('/log/:project', controllers.createLog);
}