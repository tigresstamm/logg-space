var mongoose = require('mongoose');

module.exports = {
  Log: mongoose.model('Log', {
    project: String,
    line: String,
    timestamp: Date,
    created: Date
  }),
  User: mongoose.model('User', {
    created: Date,
    username: String,
    password: String,
  }),
  Project: mongoose.model('Project', {
    created: Date,
    id: Number,
    name: String,
  })
};