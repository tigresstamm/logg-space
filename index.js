var restify = require('restify');
const controllers = require('./controllers');
const routes = require('./routes');

var server = restify.createServer();

server.use(restify.plugins.bodyParser({ mapParams: true }));

server.listen(1066, function() {
  console.log('%s listening at %s', server.name, server.url);
});
routes(server);
var mongoose = require('mongoose');
var promise = mongoose.connect('mongodb://localhost/loggspace', {
  useMongoClient: true,
});