const models = require('../models');
const Log = models.Log;
const User = models.User;
const Project = models.Project;

module.exports = {
  createLog: (req, res, next) => {
    console.log(req.params);
    var logLine = new Log({
      project: req.params.project,
      line: req.params.content,
      timestamp: new Date(req.params.timestamp),
      created: new Date()
    });

    logLine.save(function (err) {
      if (err) {
        res.send(err);
        console.log(err);
      } else {
        res.send('logged to ' + req.params.project);
        console.log('logged to ' + req.params.project);
      }
    });

    next();
  },
  getLogs: (req, res, next) => {
    Log.find(function (err, logs) {
      if (err) {
        res.send(err);
        console.log(err);
      } else {
        res.send(logs);
        console.log('read logs from ' + req.params.project);
      }
    });

    next();
  },
  getUsers: (req, res, next) => {
    User.find(function (err, users) {
      if (err) {
        res.send(err);
        console.log(err);
      } else {
        res.send(users);
        console.log('get all users ' + req.params.project);
      }
    });

    next();
  },
  getUser: (req, res, next) => {
    next();
  },
  createUser: (req, res, next) => {
    console.log(req.params);
    var newUser = new User({
      created: new Date(),
      username: req.params.username,
      password: req.params.password,
    });

    newUser.save(function (err) {
      if (err) {
        res.send(err);
        console.log(err);
      } else {
        res.send('created user ' + req.params.username);
        console.log('created user ' + req.params.username);
      }
    });

    next();
  },
  getProjects: (req, res, next) => {
    next();
  },
  getProject: (req, res, next) => {
    next();
  },
  createProject: (req, res, next) => {
    next();
  }
}
